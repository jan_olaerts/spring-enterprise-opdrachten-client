package be.janolaerts.enterprisespringopdrachten.client;

import be.janolaerts.enterprisespringopdrachten.wsimported.Beer;
import be.janolaerts.enterprisespringopdrachten.wsimported.BeerServiceEndpoint;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.jaxws.JaxWsPortProxyFactoryBean;

import java.net.MalformedURLException;
import java.net.URL;

@SpringBootApplication
public class BeerWSClientApp {

    // Todo: ask Ward: Not a valid service, wsimport from directory not working, in wsdl file service does not have <wsdl:service name="">

    @Bean
    public JaxWsPortProxyFactoryBean beerService() throws MalformedURLException {

        JaxWsPortProxyFactoryBean proxy =
                new JaxWsPortProxyFactoryBean();

        proxy.setServiceInterface(BeerServiceEndpoint.class);
        proxy.setWsdlDocumentUrl(new URL("http://localhost/BeerService?wsdl"));
        proxy.setNamespaceUri("http://services.enterprisespringopdrachten.janolaerts.be");
        proxy.setServiceName("BeerService");
        proxy.setPortName("BeerServiceEndpointPort");
        return proxy;
    }

    public static void main(String[] args) throws Exception {

        ConfigurableApplicationContext ctx =
                SpringApplication.run(BeerWSClientApp.class, args);

        BeerServiceEndpoint beerService = ctx.getBean("beerService", BeerServiceEndpoint.class);

        Beer beer = beerService.getBeerById(22);
        System.out.println(beer.getName());
        System.out.println(beer);
        System.out.println(beer.getBrewer().getName());
        System.out.println(beer.getCategory().getName());
        int orderNr = beerService.orderBeer("Homer", 22, 1);
        System.out.println("Order number: " + orderNr);
    }
}